<?php
/**
 * Created by PhpStorm.
 * User: fans
 * Date: 05.10.16
 * Time: 14:42
 */

namespace Fandom\Lotinfo;


use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\SectionTable;
use Bitrix\Main\Loader;

class ResidentialCompound
{
    private $newBildingsSection = 154;
    private $iblockId = '';
    private $residentialCompoundProp = 'PROP_RC_LOTINFO';
    private $residentialCompoundPropId = 76;
    private $residentialPointer = 'UF_RESIDENTIAL';
    const MODULE_NAME = "fandom.lotinfo";
    public $messages = '';
    public $errors = '';

    public function __construct(int $sectionId)
    {
        $this->newBildingsSection = $sectionId;
        $this->iblockId = \COption::GetOptionInt(self::MODULE_NAME, 'IBLOCK_ID');

        Loader::includeModule('iblock');
    }

    public function actualizeResidentialCompounds()
    {
        $resIds = $this->getResidentialIds();
        $absentIds = $this->getAbsentsIds($resIds);

        $this->getResCompoundsFromLotInfo($absentIds);
    }

    private function getResCompoundsFromLotInfo(array $ids)
    {
        if (empty($ids)) return;

        foreach ($ids as $id) {
            $c = Curl::getFile(
                \COption::GetOptionString(self::MODULE_NAME, 'XML_FILE_RES_COMPS'),
                $_SERVER['DOCUMENT_ROOT'] . \COption::GetOptionString(self::MODULE_NAME, 'XML_DIR_RES_COMP'),
                $id,
                \COption::GetOptionString(self::MODULE_NAME, 'API_URL'),
                [
                    'apiKey' => \COption::GetOptionString(self::MODULE_NAME, 'API_KEY'),
                    'cmd' => \COption::GetOptionString(self::MODULE_NAME, 'API_CMD_RESIDENTIAL_COMPOUNDS'),
                    'artId' => $id,
                ]
            );
        }

        return;
    }

    private function getAbsentsIds(array $resIds): array
    {
        $result = [];

        if (empty($resIds)) {
            return $resIds;
        }

        $ob = \CIBlockSection::GetList(
            [],
            [
                'IBLOCK_ID' => $this->iblockId,
                'SECTION_ID' => $this->newBildingsSection,
                'XML_ID' => $resIds,
                $this->residentialPointer => true
            ],
            false,
            [
                'IBLOCK_ID',
                'XML_ID',
                'ID'
            ]
        );

        while ($res = $ob->Fetch()) {
            if ($res['XML_ID']) $result[] = $res['XML_ID'];
        }

        return array_diff($resIds, $result);
    }

    public function getResidentialIds(): array
    {
        $res = [];
        $ob = \CIBlockElement::GetPropertyValues(
            $this->iblockId,
            [
                '!PROPERTY_' . $this->residentialCompundProp => 'false',
                'ACTIVE' => 'Y',
                'SECTION_ID' => $this->getSectionsIds()
            ],
            false,
            [
                'CODE' => $this->residentialCompoundProp
            ]
        );

        while ($result = $ob->Fetch()) {
            if ($result[$this->residentialCompoundPropId])
                $res[$result[$this->residentialCompoundPropId]] = $result[$this->residentialCompoundPropId];
        }

        $res = array_unique($res);

        return $res;
    }

    private function getSectionsIds(): array
    {
        $res[] = $this->newBildingsSection;

        $ob = SectionTable::getList(
            [
                'select' => [
                    'ID'
                ],
                'filter' => [
                    'IBLOCK_SECTION_ID' => $this->newBildingsSection,
                ]
            ]
        );

        while ($result = $ob->fetch()) {
            if ($result['ID']) $res[] = $result['ID'];
        }

        return $res;
    }

    public function addResidentialCompounds()
    {
        $resCompXmlDir = $_SERVER['DOCUMENT_ROOT'] . \COption::GetOptionString(self::MODULE_NAME, 'XML_DIR_RES_COMP');

        if (!is_dir($resCompXmlDir)) {
            $this->redError('dir for res comps xml files is absent');
            return;
        }

        $resCompXmlFiles = array_diff(scandir($resCompXmlDir), ['.', '..']);

        if (count(scandir($resCompXmlDir)) == 0) {
            $this->messages .= \Helper::boldColorText('no new residential compounds', 'green');
            return;
        };

        $resComps = $this->getNewResidentials($resCompXmlDir, $resCompXmlFiles);

        if (empty($resComps)) return;

        $newResComps = $this->addNewResidentials($resComps);

        if (empty($newResComps)) return;

        $this->updateObjects($newResComps);

        Common::recRMDir($resCompXmlDir);

        return;
    }

    private function updateObjects(array $resComps)
    {
        $ob = \CIBlockElement::GetList(
            ['ID' => 'ASC'],
            [
                'IBLOCK_ID' => $this->iblockId,
                '!PROPERTY_' . $this->residentialCompoundProp => false
            ],
            false,
            false,
            [
                'ID',
                'IBLOCK_SECTION_ID',
                'PROPERTY_' . $this->residentialCompoundProp
            ]
        );

        while ($res = $ob->Fetch()) {
            if ($newSectionId = $resComps[$res['PROPERTY_' . $this->residentialCompoundProp . '_VALUE']]) {
                $this->setObjectSections($res['ID'], $newSectionId);
            }
        }

    }

    private function setObjectSections(int $id, int $resCompSectId)
    {
        $result = [];
        $ob = \CIBlockElement::GetElementGroups($id, true, ['ID']);

        while ($res = $ob->Fetch()) {
            $result[] = $res['ID'];
        }

        if (!empty($result) && !in_array($resCompSectId, $result)) {
            $result[] = $resCompSectId;
            \CIBlockElement::SetElementSection($id, $result);
        }
    }

    private function addNewResidentials(array $resComps): array
    {
        $res = [];

        $newSection = new \CIBlockSection();

        foreach ($resComps as $resComp) {
            $fields = array(
                'ACTIVE' => 'Y',
                'IBLOCK_SECTION_ID' => $this->newBildingsSection,
                'IBLOCK_ID' => $this->iblockId,
                'NAME' => $resComp['name'],
                'CODE' => $resComp['code'],
                'XML_ID' => $resComp['xml_id'],
                $this->residentialPointer => true
            );

            $id = $newSection->Add($fields);

            if($id){
                $res[$resComp['xml_id']] = $id;
                $this->messages .= \Helper::boldColorText('Добавлен жилой комплекс ' . $id);
            }else{
                $this->errors .= \Helper::boldColorText('Добавление жилого комплекса не удалось:' . $resComp['name'] . ' не удалось((( - ' . $newSection->LAST_ERROR, 'red');
            }
        }

        return $res;
    }

    private function getNewResidentials(string $dir, array $files): array
    {
        $res = [];

        foreach ($files as $file) {
            $xml_str = file_get_contents($dir . '/' . $file);

            $xml = new \CDataXML();
            $xml->LoadString($xml_str);
            $arXml = $xml->GetArray();

            if (
                $arXml['article']['#']['id'][0]['#'] &&
                $name = $arXml['article']['#']['thread'][0]['#']
            ) {
                $res[] = [
                    'xml_id' => $arXml['article']['#']['id'][0]['#'],
                    'name' => $name,
                    'code' => \php_rutils\RUtils::translit()->slugify($name)
                ];
            }
        }

        return $res;
    }

    private function redError($err)
    {
        $this->errors .= \Helper::boldColorText($err, 'red');
    }
}
