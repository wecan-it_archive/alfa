<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

$MESS['REFERENCES_OPTIONS_RESTORED'] = "Восстановлены настройки по умолчанию";
$MESS['REFERENCES_OPTIONS_SAVED'] = "Настройки сохранены";
$MESS['REFERENCES_INVALID_VALUE'] = "Введено неверное значение";
$MESS['DEBUG'] = "Тестовый режим";
$MESS['TMP_DIR'] = "Директория для временных файлов";
$MESS['XML_FILE'] = "Шаблон для xml файлов";
$MESS['XML_FILE_RES_COMPS'] = "Шаблон для xml файлов жилых комплексов";
$MESS['XML_DIR'] = "Директория для файлов из Лотинфо";
$MESS['XML_DIR_RES_COMP'] = "Директория для файлов жилых комплексов из Лотинфо";
$MESS['LOG_FILE'] = "<a href='' target='_blank' id='LOG_FILE_LINK'>Файл логов</a>";
$MESS['API_URL'] = "API Url";
$MESS['API_KEY'] = "API Key";
$MESS['API_CMD'] = "Команда API";
$MESS['API_CMD_RESIDENTIAL_COMPOUNDS'] = "Команда API для жилых комплексов";
$MESS['GET_PARAMS'] = "GET параметры";
$MESS['GET_PARAMS_RES_COMPS'] = "GET параметры для жилых комплексов";
$MESS['IBLOCK_ID'] = "Инфоблок";
$MESS['SITIES_ID'] = "Инфоблок городов";
$MESS['TAB_TWO'] = "Свойства";
$MESS['TAB_TWO_TITLE'] = "Соответствия полей из ЛотиИнфо свойствам на сайте";
$MESS['LOTINFO_FIELD'] = "Поле в ЛотИнфо";
$MESS['PROP_ID'] = "Поле на сайте";
