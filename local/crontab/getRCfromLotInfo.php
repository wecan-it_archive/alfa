<?
//die();
error_reporting(E_ALL);
$_SERVER["DOCUMENT_ROOT"] = preg_replace('/\/\w*\/\w*\/\w*\.php$/', '', __FILE__);
$domen = 'http://www.alfa-74.ru';

if (!is_dir($_SERVER["DOCUMENT_ROOT"]))
    die("DOCUMENT_ROOT - notDir");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Fandom\Lotinfo;

if (!\Bitrix\Main\Loader::includeModule("fandom.lotinfo"))
    die("Не удалось загрузить модуль fandom.lotinfo");

$i = new Lotinfo\ResidentialCompound(154);
$i->actualizeResidentialCompounds();