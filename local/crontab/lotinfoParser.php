<?
//die();
error_reporting(E_ALL);

putenv("MAGICK_THREAD_LIMIT=2");
Imagick::setResourceLimit(6, 2);

$_SERVER["DOCUMENT_ROOT"] = preg_replace('/\/\w*\/\w*\/\w*\.php$/', '', __FILE__);
$domen = 'https://www.alfa-74.ru';

if (!is_dir($_SERVER["DOCUMENT_ROOT"]))
    die("DOCUMENT_ROOT - notDir");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Fandom\Lotinfo;

if (!\Bitrix\Main\Loader::includeModule("fandom.lotinfo"))
    die("Не удалось загрузить модуль fandom.lotinfo");

try {
    $parsing = new Lotinfo\Parser(
        $_SERVER["DOCUMENT_ROOT"],
        $argv[1],
        $argv[2]
    );

    $pars = $parsing->importData();
    $logFile = $parsing->arParams['LOG_FILE'];
    $text = '';
    if (!empty($parsing->message)) {
        $messages = \Helper::boldColorText("Messages: ", "black");
        $text .= $messages . $parsing->message;
    }
    if (!empty($parsing->errors)) {
        $errors = \Helper::boldColorText("Errors: ", "black");
        $text .= $errors . $parsing->errors;
    }

    file_put_contents($logFile, $text, FILE_APPEND);
    if ($argv[2] && $argv[2] == 2) {
        $link = $domen . str_replace($_SERVER['DOCUMENT_ROOT'], '', $parsing->arParams['LOG_FILE']);
        $mailText = '
            Dear Inzilya, you can visit <a href="' . $link . '">this link</a> to see the parsing report
        ';
        Lotinfo\Common::sendMail($mailText, "parsing", true);
    }
} catch (Exception $e) {
    echo $e->getMessage();
}
