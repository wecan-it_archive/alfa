<?php

if(!php_sapi_name() == 'cli') die;

$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__) . '/../../');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('dhlebin.yandexrealtyexport');

$export = new \Dhlebin\YandexRealtyExport\Export();
try {
    $export->generate();
} catch (Exception $e) {
    echo $e->getMessage();
    mail(
        'kran1lavaz@gmail.com',
        'Выгрузка в яндекс недвижимость провалилась',
        $e->getMessage() . "\n" . $e->getTraceAsString()
    );
}
