<?
$_SERVER["DOCUMENT_ROOT"] = preg_replace('/\/\w*\/\w*\/\w*\.php$/', '', __FILE__);

if (!is_dir($_SERVER["DOCUMENT_ROOT"]))
    die("DOCUMENT_ROOT - notDir");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!\Bitrix\Main\Loader::includeModule("fandom.subscription"))
    die("Не удалось загрузить модуль fandom.subscription");

if (!\Bitrix\Main\Loader::includeModule("iblock"))
    die("Не удалось загрузить модуль iblock");

$mess = '<h2 style="color:black">Подписка</h2>';
try {
    $subscr = new Fandom\Subscription\Mailout();
    $res = $subscr->doIt();

    if ($res && $logFile = \COption::GetOptionString('fandom.subscription', 'LOG_FILE')) {
        $mess .= $subscr->message;
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . $logFile, $subscr->message, FILE_APPEND);
    }
} catch (Exception $e) {
    if ($logFile = \COption::GetOptionString('fandom.subscription', 'LOG_FILE')) {
        $mess .= $e->getMessage();
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . $logFile, $mess, FILE_APPEND);
    }
}