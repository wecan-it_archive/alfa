<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
$curPage = $APPLICATION->GetCurPage(false);
?>
<?
if ($curPage != "/"):
?>
		</div>
	</main>
<?
endif;
?>
<footer class="page-footer">
	<nav class="navigation">
		<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_common", Array(
	"COMPONENT_TEMPLATE" => "top",
		"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
		"MENU_CACHE_TYPE" => "A",	// Тип кеширования
		"MENU_CACHE_TIME" => "360000",	// Время кеширования (сек.)
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
	),
	false
);?>
		<ul class="social">
            <? Helper::includeFile('social');?>
		</ul>
	</nav>
	<div class="page-footer__inner">
        <? Helper::includeFile('footer_adress');?>
	</div>
	<div class="page-footer__wrapper">
        <?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "footer", Array(
	"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "objects",	// Тип инфоблока
		"IBLOCK_ID" => "5",	// Инфоблок
		"SECTION_ID" => $_REQUEST["SECTION_ID"],	// ID раздела
		"SECTION_CODE" => "",	// Код раздела
		"COUNT_ELEMENTS" => "N",	// Показывать количество элементов в разделе
		"TOP_DEPTH" => "1",	// Максимальная отображаемая глубина разделов
		"SECTION_FIELDS" => array(	// Поля разделов
			0 => "",
			1 => "",
		),
		"SECTION_USER_FIELDS" => array(	// Свойства разделов
			0 => "",
			1 => "",
		),
		"VIEW_MODE" => "LINE",	// Вид списка подразделов
		"SHOW_PARENT_NAME" => "Y",	// Показывать название раздела
		"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
	),
	false
);?>
	</div>
	<div class="page-footer__form">
        <?
        $APPLICATION->IncludeComponent("bitrix:search.form", "alfa", Array(
	"PAGE" => "#SITE_DIR#search/",	// Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
	),
	false
);
        ?>
		<div class="footer-creator"><!-- @ Radiant Content--></div>
        <div class="footer-creator"><a href="http://wecan-it.ru"><span>Разработка</span><img src="/local/templates/.default/img/logo-alfa.png"></a></div>
	</div>
</footer>
</div>
<div class="popup js-popup--img">
	<div class="popup__wrapper">
		<div class="popup__item">
			<div class="popup__inner">
			</div>
			<a href="javascript:void(0)" class="popup__close js-popup-close">X</a>
			<div class="popup__container">
				<img src="<?=DEFAULT_TEMPLATE_PATH;?>/img/certificate-1--big.jpg" alt="" class="popup__img">
			</div>
		</div>
	</div>
</div>
<div class="popup popup--ask-call">
	<div class="popup__wrapper">
		<div class="popup__item">
			<?
			$APPLICATION->IncludeComponent(
				"fandom:iblock.element.add.xform",
				"call_order",
				array(
					"COMPONENT_TEMPLATE" => "call_order",
					"STATUS_NEW" => "N",
					"LIST_URL" => "",
					"USE_CAPTCHA" => "N",
					"USER_MESSAGE_EDIT" => "",
					"USER_MESSAGE_ADD" => "Наш менеджер свяжется с вами через некоторое время",
					"DEFAULT_INPUT_SIZE" => "30",
					"RESIZE_IMAGES" => "N",
					"IBLOCK_TYPE" => "common",
					"IBLOCK_ID" => "7",
					"PROPERTY_CODES" => array(
						0 => "17",
						1 => "NAME",
					),
					"PROPERTY_CODES_REQUIRED" => array(
						0 => "17",
						1 => "NAME",
					),
					"RULES" => "",
					"GROUPS" => array(
						0 => "2",
					),
					"STATUS" => "ANY",
					"ELEMENT_ASSOC" => "CREATED_BY",
					"MAX_USER_ENTRIES" => "100000",
					"MAX_LEVELS" => "100000",
					"LEVEL_LAST" => "Y",
					"MAX_FILE_SIZE" => "0",
					"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
					"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
					"SEF_MODE" => "N",
					"AJAX_MODE" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "N",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"CUSTOM_TITLE_NAME" => "Как к вам обращаться?",
					"CUSTOM_TITLE_TAGS" => "Заказать звонок",
					"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
					"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
					"CUSTOM_TITLE_IBLOCK_SECTION" => "",
					"CUSTOM_TITLE_PREVIEW_TEXT" => "",
					"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
					"CUSTOM_TITLE_DETAIL_TEXT" => "",
					"CUSTOM_TITLE_DETAIL_PICTURE" => ""
				),
				false
			);
			?>
		</div>
	</div>
</div>

<div class="popup popup--add-property">
	<div class="popup__wrapper">
		<div class="popup__item">
            <?$APPLICATION->IncludeComponent(
	"fandom:iblock.element.add.xform", 
	"add_object", 
	array(
		"COMPONENT_TEMPLATE" => "add_object",
		"STATUS_NEW" => "N",
		"LIST_URL" => "",
		"USE_CAPTCHA" => "Y",
		"USER_MESSAGE_EDIT" => "",
		"USER_MESSAGE_ADD" => "Наш менеджер свяжется с вами через некоторое время",
		"DEFAULT_INPUT_SIZE" => "30",
		"RESIZE_IMAGES" => "Y",
		"IBLOCK_TYPE" => "common",
		"IBLOCK_ID" => "17",
		"PROPERTY_CODES" => array(
			0 => "54",
			1 => "55",
			2 => "56",
			3 => "57",
			4 => "NAME",
		),
		"PROPERTY_CODES_REQUIRED" => array(
			0 => "55",
			1 => "NAME",
		),
		"RULES" => "",
		"GROUPS" => array(
			0 => "2",
		),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"MAX_USER_ENTRIES" => "100000",
		"MAX_LEVELS" => "100000",
		"LEVEL_LAST" => "Y",
		"MAX_FILE_SIZE" => "0",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"SEF_MODE" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CUSTOM_TITLE_NAME" => "Как к вам обращаться?",
		"CUSTOM_TITLE_TAGS" => "Добавить свой объект",
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "Описание:",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "",
		"CUSTOM_TITLE_DETAIL_PICTURE" => ""
	),
	false
);
            ?>
		</div>
	</div>
</div>
<div class="popup popup--write">
	<div class="popup__wrapper">
		<div class="popup__item">
			<?$APPLICATION->IncludeComponent(
				"fandom:iblock.element.add.xform",
				"send_questions",
				array(
					"COMPONENT_TEMPLATE" => "send_questions",
					"STATUS_NEW" => "N",
					"LIST_URL" => "",
					"USE_CAPTCHA" => "Y",
					"USER_MESSAGE_EDIT" => "",
					"USER_MESSAGE_ADD" => "Наш менеджер свяжется с вами через некоторое время",
					"DEFAULT_INPUT_SIZE" => "30",
					"RESIZE_IMAGES" => "N",
					"IBLOCK_TYPE" => "common",
					"IBLOCK_ID" => "8",
					"PROPERTY_CODES" => array(
						0 => "18",
						1 => "19",
						2 => "NAME",
						3 => "PREVIEW_TEXT",
					),
					"PROPERTY_CODES_REQUIRED" => array(
						0 => "NAME",
						1 => "PREVIEW_TEXT",
					),
					"RULES" => "",
					"GROUPS" => array(
						0 => "2",
					),
					"STATUS" => "ANY",
					"ELEMENT_ASSOC" => "CREATED_BY",
					"MAX_USER_ENTRIES" => "100000",
					"MAX_LEVELS" => "100000",
					"LEVEL_LAST" => "Y",
					"MAX_FILE_SIZE" => "0",
					"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
					"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
					"SEF_MODE" => "N",
					"AJAX_MODE" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "N",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"CUSTOM_TITLE_NAME" => "Как к вам обращаться?",
					"CUSTOM_TITLE_TAGS" => "Написать письмо",
					"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
					"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
					"CUSTOM_TITLE_IBLOCK_SECTION" => "",
					"CUSTOM_TITLE_PREVIEW_TEXT" => "Текст заявки:",
					"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
					"CUSTOM_TITLE_DETAIL_TEXT" => "",
					"CUSTOM_TITLE_DETAIL_PICTURE" => ""
				),
				false
			);
			?>
		</div>
	</div>
</div>
<a href="/ipotechnyy-kalkulyator/" class="side-stick side-stick--calc">Ипотечный калькулятор</a>
<noindex>
    <a href="http://reestr.rgr.ru/" rel="nofollow" class="side-stick side-stick--check" target="_blank">Проверьте своего риэлтора</a>
</noindex>
<? Helper::includeFile('body-scripts');?>
<noindex><a id="href4" target="_blank" rel="nofollow" href="http://9827468.informerra.ru"><img class="anchorImage" alt="альфа, агентство недвижимости" src="http://www.informerra.ru/img/i.png"></a></noindex>
<?// Helper::includeFile('banner');?>

</body>
</html>