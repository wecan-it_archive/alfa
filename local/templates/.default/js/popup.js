(function() {
    $(document).ready(function(){
        $(document).on('change', '.agreement_checkbox', function () {
            var buttons = $(this).closest('form').find('input[type=submit], input[name=submitbutton]');
            if(this.checked)
                $(buttons).prop('disabled', false);
                //$(buttons).removeClass('disabled');
            else
                $(buttons).prop('disabled', true);
                //$(buttons).addClass('disabled');
        });
        /*$('form input[type="submit"]').on('click', function(e){
            if(!$('input[type="checkbox"].agreement_checkbox').prop('checked')) {
                e.preventDefault();
                console.log('submit');
                return false;
            }
        });*/


        $(".fancybox").fancybox({
            openEffect	: 'none',
            closeEffect	: 'none',
            helpers : {
                title : {
                    type : 'inside'
                }
            }
        });

        var popup_call = document.querySelector(".popup--ask-call");

        $('.page-header__ask-call').live('click', function(e) {
            e.preventDefault();
            e = e || window.event;
            popup_call.classList.add("popup--active");
            $(popup_call.getElementsByClassName("popup__container--success")).css("display", "block");
            Recaptchafree.reset();

        });

        var popup_broker = document.querySelector(".popup--broker_consult");

        $('.page-header__broker_consult-call').live('click', function(e) {
            e.preventDefault();
            e = e || window.event;
            popup_broker.classList.add("popup--active");
            $(popup_broker.getElementsByClassName("popup__container--success")).css("display", "block");
            Recaptchafree.reset();

        });

        var object_viewing = document.querySelector(".popup--object_viewing");

        $('.page-header__object_viewing-call').live('click', function(e) {
            e.preventDefault();
            e = e || window.event;
            object_viewing.classList.add("popup--active");
            $(object_viewing.getElementsByClassName("popup__container--success")).css("display", "block");
            Recaptchafree.reset();

        });


        $('.page-header__subscription-call').live('click', function(e) {
            var subscription = document.querySelector(".popup--subscription");
            e.preventDefault();
            e = e || window.event;
            subscription.classList.add("popup--active");
            $(subscription.getElementsByClassName("popup__container--success")).css("display", "block");
            Recaptchafree.reset();

        });

        var mistake = document.querySelector(".report-mistake");
        var popup_mistake = document.querySelector(".popup--mistake");

        if (mistake) {
            mistake.addEventListener("click", function(e) {
                e.preventDefault();
                e = e || window.event;
                popup_mistake.classList.add("popup--active");
                $(popup_mistake.getElementsByClassName("popup__container--success")).css("display", "block");
            }, false);
        }

        var ask = document.querySelector(".page-header__ask");
        var popup_ask = document.querySelector(".popup--write");

        if (ask) {
            ask.addEventListener("click", function(e) {
                e.preventDefault();
                e = e || window.event;
                popup_ask.classList.add("popup--active");
                $(popup_ask.getElementsByClassName("popup__container--success")).css("display", "block");
                Recaptchafree.reset();
            }, false);

        }

        $('.popup__item').on("click", '.js-popup-close', function (e) {
            var popup_active = document.querySelectorAll(".popup--active");
            for (i = 0; i < popup_active.length; ++i){
                $(popup_active[i]).removeClass("popup--active");
            }
        });

        var add = document.querySelectorAll(".filter-build__add");
        var popup_add = document.querySelector(".popup--add-property");

        if (add) {
            $(add).each(function(index, item) {
                add[index].addEventListener("click", function(e) {
                    e.preventDefault();
                    e = e || window.event;
                    popup_add.classList.add("popup--active");
                    $(popup_add.getElementsByClassName("popup__container--success")).css("display", "block");
                    Recaptchafree.reset();

                }, false)
            });
        }
    });
})();

(function(){
    var estate_img = document.querySelectorAll(".js-estate-img");
    pop_img(estate_img);

    var review = document.querySelectorAll(".review__img-link");
    pop_img(review);

    var popup_pics = document.querySelector(".js-popup--img");
    var popup_img = document.querySelector(".popup__img");

    function pop_img(img) {
        for (i = 0; i < img.length; ++i) {
            img[i].addEventListener("click", function(e) {
                e.preventDefault();
                e = e || window.event;
                var target = e.target || e.srcElement;
                if (target.tagName === "IMG")
                    popup_pics.classList.add("popup--active");
                popup_img.setAttribute("src", target.parentElement.getAttribute("href"));
            }, false);
        }
    }
})();
