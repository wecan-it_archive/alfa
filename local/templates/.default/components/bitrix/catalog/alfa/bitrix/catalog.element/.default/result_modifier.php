<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */

//dump($arParams)
$props = [
    'PROP_THREE_D' => 5,
    'PROP_INFO' => 13
];

if ($images = $arResult['DISPLAY_PROPERTIES']['PROP_IMAGES']['FILE_VALUE']) {
    if ($images['ID']) $images[] = $images;
    
    foreach ($images as $key=>$image) {
        if ($key == 0)
            $arSize = ['width' => 600, 'height' => 500];
        else
            $arSize = ['width' => 287, 'height' => 431];

        $pic = CFile::ResizeImageGet($image['ID'], $arSize, BX_RESIZE_IMAGE_PROPORTIONAL);

        if ($pic) {
            $arResult["IMAGES"][] = [
                'MIN' => $pic['src'],
                'BIG' => $image['SRC']
            ];
        }
    }
}

$arResult['DISTRICT'] = BitrixMethod::getSections(
    [
        'ID' => $arResult['IBLOCK_SECTION_ID'],
        'IBLOCK_ID' => $arResult['IBLOCK_ID'],
    ],
    [
        'ID',
        'NAME',
        'CODE',
        'UF_RESIDENTIAL',
        'SECTION_ID'
    ]
);

if ($threeD = $arResult['PROPERTY_' . $props['PROP_THREE_D']]) {
    $cp = $this->__component; // объект компонента

    if (is_object($cp))
    {
        $cp->arResult['THREE_D'] = true;
        $cp->SetResultCacheKeys(array('THREE_D'));
        $arResult['IS_OBJECT'] = $cp->arResult['THREE_D'];
    }
}

$arResult['RIELTOR'] = [];

if ($reiltor = $arResult['PROPERTIES']['PROP_NAME_OF_REILTOR']['VALUE']) {
    $lastName = explode(' ', $reiltor)[0];
    if ($lastName) {
        $reiltorOb = CIBlockElement::GetList(
            [],
            [
                'IBLOCK_ID' => $employeeIblock,
                '%NAME' => $lastName,
            ],
            false,
            false,
            [
                'ID',
                'IBLOCK_ID',
                'NAME',
                'PROPERTY_*',
                //'PROPERTY_PROP_PHONE',
                'PREVIEW_PICTURE',
                'DETAIL_PAGE_URL',
            ]
        );
        if ($ob = $reiltorOb->GetNextElement()) {
            $arRieltor = $ob->GetFields();
            $arResult['RIELTOR']['NAME'] = $arRieltor['NAME'];
            $arResult['RIELTOR']['DETAIL_PAGE_URL'] =
                str_replace(['#SITE_DIR#', '#ELEMENT_CODE#'], ['', $arRieltor['CODE']], $arRieltor['DETAIL_PAGE_URL']);
            if ($arRieltor['PREVIEW_PICTURE']) {
                $pic = CFile::ResizeImageGet(
                    $arRieltor['PREVIEW_PICTURE'],
                    ['width' => 196, 'height' => 281],
                    BX_RESIZE_IMAGE_PROPORTIONAL
                );

                if ($pic) {
                    $arResult['RIELTOR']['PIC'] = $pic['src'];
                }
            }

            $arProps = $ob->GetProperties();
            if ($post = $arProps['PROP_POST']['VALUE']) $arResult['RIELTOR']['PROPERTY_PROP_POST_VALUE'] = $post;
            if ($phone = $arProps['PROP_PHONE']['VALUE']) $arResult['RIELTOR']['PROPERTY_PROP_PHONE_VALUE'] = $phone;
        }
    }
}

if($val = &$arResult['PROPERTY_' . $props['PROP_INFO']]['TEXT']) {
    $n = 2;
    $i = 0;
    $new_text = '<p class="estate-card__text">';

    $text = explode('. ', trim($val));
    echo '<pre style="display:none;">';print_r($text);echo '</pre>';
    if(is_array($text)){
        $count = count($text);

        foreach($text as $arText){
            $new_text .= htmlspecialcharsBack($arText);
            if($i != ($count-1)){
                $new_text .= '. ';
            }

            if($i != 0 and ($i+1)%$n == 0){
                $new_text .= '</p><p class="estate-card__text">';
            }

            $i++;
        }
    }

    $new_text .= '</p>';
    $val = $new_text;
}
echo '<pre style="display:none;">';print_r($val);echo '</pre>';
// trans text
$mainSection = BitrixMethod::getSections(
    [
        'IBLOCK_ID' => $arParams['IBLOCK_ID'],
        'ID' => $arResult['SECTION']['IBLOCK_SECTION_ID'],
    ],
    [
        'ID',
        'UF_PRANSPARENT_DOWN',
    ]
);

if (!empty($mainSection)) {
    if ($transText = $mainSection[$arResult['SECTION']['IBLOCK_SECTION_ID']]['UF_PRANSPARENT_DOWN']) {
        $this->SetViewTarget('trans_text');
        echo $transText;
        $this->EndViewTarget();
    }
}

$itemSections = [];
$ob = CIBlockElement::GetElementGroups($arResult['ID'], true, ['ID']);
while ($res = $ob->Fetch()) {
    $itemSections[] = $res['ID'];
}

if (!empty($itemSections)) {
    $residence = BitrixMethod::getSections(
        [
            'IBLOCK_ID' => $arParams['IBLOCK_ID'],
            'ID' => $itemSections
        ],
        [
            'ID',
            'UF_RESIDENTIAL',
            'UF_RESIDENCE_TYPE'
        ]
    );

    if (!empty($residence)) {
        foreach ($residence as $resid) {
            if ($resid['UF_RESIDENTIAL'] && $type = $resid['UF_RESIDENCE_TYPE'])
                $arResult['UF_RESIDENCE_TYPE'] = $type;
        }
    }
}