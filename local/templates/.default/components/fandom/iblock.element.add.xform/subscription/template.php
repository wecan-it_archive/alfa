<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<?
//echo "<pre>Template arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//exit();
use \Bitrix\Main\Localization\Loc;
?>
<?
$objectPropId = 61;
$typePropId = 67;
$districtPropId = 68;

$districtId = $arResult['ELEMENT_PROPERTIES'][$districtPropId][0]['VALUE'] ?: 0;
?>
<script>
    var tree = <?=json_encode($arResult['sections']);?>;
    var districtId = <?=$districtId;?>;

    Vue.component('sections', {
        template: '#sections-temp',
        props: {
            tree: {},
            selected: 1,
            selected_district: 1,
            errors: {}
        },
        data: function () {
            return
        },
        methods: {}
    })
    $(function () {
        var vm = new Vue({
            el: '#tree',
            data: {
                tree: tree,
                selected: <?=$arResult['ELEMENT_PROPERTIES'][$typePropId][0]['VALUE']?: 0;?>,
                selected_district: districtId,
                errors: {
                    type: '<?=$arResult['ERRORS'][$typePropId] ? 'red' : '';?>',
                    district: '<?=$arResult['ERRORS'][$districtPropId] ? 'red' : '';?>'
                }
            },
            created: function () {
            }
        });
    })
</script>
<script type="x/template" id="sections-temp">
    <div class="calc__wrapper">
        <span class="calc__text" v-bind:style="{ color: errors.type}">Тип недвижимости</span>
        <ul class="filter__container--middish filter__dropdown--calc filter__dropdown--no-js">
            <li>
                <span class="filter__dropdown-control" v-text="selected ? tree[selected].NAME : 'Выбрать'"></span>
                <ul class="filter__dropdown">
                    <li class="filter__item" v-for="list in tree">
                        <label>
                            <input class="filter__check"
                                   type="radio"
                                   v-model="selected" v-bind:value="list.ID" name="PROPERTY[<?=$typePropId?>][0]">
                            <span>{{ list.NAME }}</span>
                        </label>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="calc__wrapper" v-if="selected">
        <span class="calc__text" v-bind:style="{ color: errors.district}">Район</span>
        <ul class="filter__container--middish filter__dropdown--calc filter__dropdown--no-js">
            <li>
                <span class="filter__dropdown-control"
                      v-text="(selected_district && tree[selected].sections[selected_district]) ? tree[selected].sections[selected_district].NAME : 'Выбрать'"></span>
                <ul class="filter__dropdown">
                    <li class="filter__item" v-for="section in tree[selected].sections">
                        <label>
                            <input class="filter__check" type="radio" v-bind:value="section.ID"
                                   v-model="selected_district" name="PROPERTY[<?=$districtPropId?>][0]">
                            <span>{{ section.NAME }}</span>
                        </label>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</script>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
    <div class="popup__container popup__container--success popup--active">
        <div class="popup__success"></div>
        <p class="calc__text--center"><?=$arResult["MESSAGE"]?></p>
        <div class="popup__form-inner">
            <div class="calc__submit-wrap">
                <a href="javascript:void(0)" class="btn--med calc__submit js-popup-close"><?=Loc::getMessage("IBLOCK_FORM_CLOSE")?></a>
            </div>
        </div>
    </div>
<? else: ?>
    <? if ($arParams['CUSTOM_TITLE_TAGS']):?>
        <div class="popup__inner">
            <div class="popup__heading">
                <?=$arParams['CUSTOM_TITLE_TAGS'];?>
            </div>
        </div>
    <? endif;?>
<a href="javascript:void(0)" class="popup__close js-popup-close">X</a>
    <form name="iblock_addd" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data" class="popup__container popup__container--form">
        <?=bitrix_sessid_post()?>
        <input type="hidden" name="form_id" value="<?=$arResult['FORM_ID']?>" />
        <?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
        <?if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])):?>
            <?foreach ($arResult["PROPERTY_LIST"] as $propertyID):?>
                <?if ($typePropId != $propertyID && $districtPropId != $propertyID):?>
                    <div class="calc__wrapper">
                        <p class="calc__text">
                            <?if (intval($propertyID) > 0):?>
                                <?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"]?>
                            <?else:?>
                                <?=!empty($arParams["CUSTOM_TITLE_".$propertyID]) ? $arParams["CUSTOM_TITLE_".$propertyID] : Loc::getMessage("IBLOCK_FIELD_".$propertyID)?>
                            <?endif?>
                        </p>
                    <?
                    if (intval($propertyID) > 0)
                    {
                        if (
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
                            &&
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
                        )
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
                        elseif (
                            (
                                $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
                                ||
                                $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
                            )
                            &&
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
                        )
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
                    }
                    elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

                    if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
                    {
                        $inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
                        $inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
                    }
                    else
                    {
                        $inputNum = 1;
                    }

                    if($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
                        $INPUT_TYPE = "USER_TYPE";
                    else
                        $INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

                    switch ($INPUT_TYPE):
                        case "USER_TYPE":
                            for ($i = 0; $i<$inputNum; $i++)
                            {
                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                {
                                    $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
                                    $description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
                                }
                                elseif ($i == 0)
                                {
                                    $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                                    $description = "";
                                }
                                else
                                {
                                    $value = "";
                                    $description = "";
                                }
                                echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"],
                                    array(
                                        $arResult["PROPERTY_LIST_FULL"][$propertyID],
                                        array(
                                            "VALUE" => $value,
                                            "DESCRIPTION" => $description,
                                        ),
                                        array(
                                            "VALUE" => "PROPERTY[".$propertyID."][".$i."][VALUE]",
                                            "DESCRIPTION" => "PROPERTY[".$propertyID."][".$i."][DESCRIPTION]",
                                            "FORM_NAME"=>"iblock_add",
                                        ),
                                    ));
                            ?><?
                            }
                        break;
                        case "TAGS":
                            $APPLICATION->IncludeComponent(
                                "bitrix:search.tags.input",
                                "",
                                array(
                                    "VALUE" => $arResult["ELEMENT"][$propertyID],
                                    "NAME" => "PROPERTY[".$propertyID."][0]",
                                    "TEXT" => 'size="'.$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"].'"',
                                ), null, array("HIDE_ICONS"=>"Y")
                            );
                            break;
                        case "HTML":
                            $LHE = new CLightHTMLEditor;
                            $LHE->Show(array(
                                'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyID."][0]"),
                                'width' => '100%',
                                'height' => '200px',
                                'inputName' => "PROPERTY[".$propertyID."][0]",
                                'content' => $arResult["ELEMENT"][$propertyID],
                                'bUseFileDialogs' => false,
                                'bFloatingToolbar' => false,
                                'bArisingToolbar' => false,
                                'toolbarConfig' => array(
                                    'Bold', 'Italic', 'Underline', 'RemoveFormat',
                                    'CreateLink', 'DeleteLink', 'Image', 'Video',
                                    'BackColor', 'ForeColor',
                                    'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
                                    'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
                                    'StyleList', 'HeaderList',
                                    'FontList', 'FontSizeList',
                                ),
                            ));
                            break;
                        case "T":
                            for ($i = 0; $i<$inputNum; $i++)
                            {

                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                {
                                    $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                                }
                                elseif ($i == 0)
                                {
                                    $value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                                }
                                else
                                {
                                    $value = "";
                                }
                            ?>
                                </div>
                                <div class="calc__wrapper">
                                    <textarea name="PROPERTY[<?=$propertyID?>][<?=$i?>]" class="popup__textarea" maxlength="<?=MAX_FORM_TEXT_LENGTH?>" rows="8" cols="40"><?=$value?></textarea>
                                    <? if(isset($arResult['ERRORS']['PREVIEW_TEXT'])):?>
                                        <span class="valid-err" id="question-textarea-error" title="<?=Loc::getMessage('IBLOCK_FORM_DETAIL_TEXT')?>"></span>
                                    <? endif?>
                            <?
                            }
                        break;

                        case "S":
                        case "N":
                            for ($i = 0; $i<$inputNum; $i++)
                            {
                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                {
                                    $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                                }
                                elseif ($i == 0)
                                {
                                    $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

                                }
                                else
                                {
                                    $value = "";
                                }
                            ?>
                            <input class="filter__input--big filter__input--calc"
                                <?=($propertyID == 69 || $propertyID == 70)? ' pattern="^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$"' : '';?>
                                   placeholder="<?=($arResult["PROPERTY_LIST_FULL"][$propertyID]["HINT"])?: ''?>"
                                <?=($propertyID == 19)? ' placeholder="вашапочта@mail.com"':'';?>
                                type="<?=($propertyID == 61)?'hidden':'text';?>"
                                name="PROPERTY[<?=$propertyID?>][<?=$i?>]" size="25" value="<?=$value?>"
                                <?=(in_array($propertyID, $arResult["PROPERTY_REQUIRED"]))? 'required':'';?>
                            />
                            <? if ($propertyID == 18):?>
                                <span class="valid-err" title="Телефон в формате +7 987 654 32 10"></span>
                            <? endif;?>
                            <?
                            if($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime"):?><?
                                $APPLICATION->IncludeComponent(
                                    'bitrix:main.calendar',
                                    '',
                                    array(
                                        'FORM_NAME' => 'iblock_add',
                                        'INPUT_NAME' => "PROPERTY[".$propertyID."][".$i."]",
                                        'INPUT_VALUE' => $value,
                                    ),
                                    null,
                                    array('HIDE_ICONS' => 'Y')
                                );
                                ?><small><?=Loc::getMessage("IBLOCK_FORM_DATE_FORMAT")?><?=FORMAT_DATETIME?></small><?
                            endif
                            ?><?
                            }
                        break;

                        case "F":
                            for ($i = 0; $i<$inputNum; $i++)
                            {
                                $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                                ?>
                    <input type="hidden" name="PROPERTY[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" value="<?=$value?>" />
                    <input type="file" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>"  name="PROPERTY_FILE_<?=$propertyID?>_<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>" />
                                <?

                                if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value]))
                                {
                                    ?>
                <input type="checkbox" name="DELETE_FILE[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" id="file_delete_<?=$propertyID?>_<?=$i?>" value="Y" /><label for="file_delete_<?=$propertyID?>_<?=$i?>"><?=Loc::getMessage("IBLOCK_FORM_FILE_DELETE")?></label>
                                    <?

                                    if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"])
                                    {
                                        ?>
                <img src="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>" height="<?=$arResult["ELEMENT_FILES"][$value]["HEIGHT"]?>" width="<?=$arResult["ELEMENT_FILES"][$value]["WIDTH"]?>" border="0" />
                                        <?
                                    }
                                    else
                                    {
                                        ?>
                <?=Loc::getMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?>
                <?=Loc::getMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b
                [<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=Loc::getMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]
                                        <?
                                    }
                                }
                            }

                        break;
                        case "L":

                            if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
                                $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
                            else
                                $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

                            switch ($type):
                                case "checkbox":
                                case "radio":

                                    //echo "<pre>"; print_r($arResult["PROPERTY_LIST_FULL"][$propertyID]); echo "</pre>";

                                    foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
                                    {
                                        $checked = false;
                                        if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                        {
                                            if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID]))
                                            {
                                                foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum)
                                                {
                                                    if ($arElEnum["VALUE"] == $key) {$checked = true; break;}
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if ($arEnum["DEF"] == "Y") $checked = true;
                                        }

                                        ?>
                        <input type="<?=$type?>" name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><label for="property_<?=$key?>"><?=$arEnum["VALUE"]?></label>
                                        <?
                                    }
                                break;

                                case "dropdown":
                                case "multiselect":
                                ?>
                        <select name="PROPERTY[<?=$propertyID?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>">
                            <option value=""><?echo Loc::getMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
                                <?
                                    if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
                                    else $sKey = "ELEMENT";

                                    foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
                                    {
                                        $checked = false;
                                        if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                        {
                                            foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum)
                                            {
                                                if ($key == $arElEnum["VALUE"]) {$checked = true; break;}
                                            }
                                        }
                                        else
                                        {
                                            if ($arEnum["DEF"] == "Y") $checked = true;
                                        }
                                        ?>
                            <option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
                                        <?
                                    }
                                ?>
                        </select>
                                <?
                                break;

                            endswitch;
                        break;
                    endswitch;?>
                    </div>
                <? endif;?>
            <?endforeach;?>
            <div id="tree">
                <sections :tree="tree" :selected="selected" :selected_district="selected_district" :errors="errors"></sections>
            </div>
            <?if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0):?>
                <div class="calc__wrapper">
                    <p class="calc__text">
                        <?=Loc::getMessage("IBLOCK_FORM_CAPTCHA_TITLE")?>
                    </p>
                    <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                </div>
                <div class="calc__wrapper">
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                </div>
                <div class="calc__wrapper">
                    <input class="filter__input--100 filter__input--calc" type="text" name="captcha_word" value="">
                    <? if(isset($arResult['ERRORS']['CAPTCHA'])):?>
                        <script>
                            Recaptchafree.reset();
                        </script>
                        <span class="valid-err" id="question-captcha-error" title="<?=$arResult['ERRORS']['CAPTCHA']?>"></span>
                    <? endif?>
                </div>
            <?endif?>
        <?endif?>
        <div class="popup__form-inner">
            <div class="calc__submit-wrap">
                <input type="submit" name="iblock_submit" value="<?=Loc::getMessage("IBLOCK_FORM_SUBMIT")?>" class="calc__submit btn--med"/>
                <?if (strlen($arParams["LIST_URL"]) > 0 && $arParams["ID"] > 0):?><input type="submit" name="iblock_apply" value="<?=Loc::getMessage("IBLOCK_FORM_APPLY")?>" /><?endif?>
                <?/*<input type="reset" value="<?=Loc::getMessage("IBLOCK_FORM_RESET")?>" />*/?>
                <?if (strlen($arParams["LIST_URL"]) > 0):?><a href="<?=$arParams["LIST_URL"]?>"><?=Loc::getMessage("IBLOCK_FORM_BACK")?></a><?endif?>
            </div>
        </div>
    </form>
<?endif?>