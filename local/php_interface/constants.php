<?php
/**
 * Created by PhpStorm.
 * User: Khadeev Fanis
 * Date: 30/09/16
 * Time: 22:06
 */
define('OBJECTS_IBLOCK_CODE', 'objects');
define('OBJECTS_IBLOCK_ID', '5');
define('SERVICES_IBLOCK_CODE', 'uslugi');
define('SERVICES_IBLOCK_ID', 14);
define('DEFAULT_TEMPLATE_PATH', '/local/templates/.default');
define('FLASH_PATH', '/flash');
define('OBJECTS_SECTION_NEWHOUSE_PROP', 'UF_NEWHOUSE');
define('MAX_FORM_TEXT_LENGTH', 500);